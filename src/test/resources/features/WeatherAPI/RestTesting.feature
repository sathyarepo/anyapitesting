Feature: Test weather API

  Scenario: weather001- Get current weather details
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the response has field "request.type" and has data "City"

  Scenario: weather002- Get historical weather details
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "historical_queries_not_supported_on_plan"

  Scenario: weather003- Get forecast weather details
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the response has field "request.type" and has data "City"

  Scenario: weather004- Validate error code 101 Invalid Access Key
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "invalid_access_key"

  Scenario: weather005- Validate HTTPS url of forecast API endpoint
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "https_access_restricted"

  Scenario: weather006- Validate error code 103 invalid API function
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "invalid_api_function"

  Scenario: weather007- Verify error code 101 missing access key
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "missing_access_key"

  Scenario: weather008- Verify error code 615 request_failed
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "request_failed"

  Scenario: weather009- Verify error code 602 no_results
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "no_results"

  Scenario: weather010- Verify error code 604 bulk_queries_not_supported_on_plan
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "bulk_queries_not_supported_on_plan"
    And ensure the error response has field "error.code" and has data "604"
    And ensure the error response has field "error.info" and has data "Your current subscription plan does not support bulk queries. Please upgrade your account to use this feature."

  Scenario: weather011- Get Current weather stats with all optional params
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "function_access_restricted"
    And ensure the error response has field "error.code" and has data "105"
    And ensure the error response has field "error.info" and has data "Access Restricted - Your current Subscription Plan does not support this API Function."

  Scenario: weather012- Get historical weather stats with all optional params
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "historical_queries_not_supported_on_plan"
    And ensure the error response has field "error.code" and has data "603"
    And ensure the error response has field "error.info" and has data "Your current subscription plan does not support historical weather data. Please upgrade your account to use this feature."

  Scenario: weather013- Get historical time series weather stats with all optional params
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "historical_queries_not_supported_on_plan"
    And ensure the error response has field "error.code" and has data "603"
    And ensure the error response has field "error.info" and has data "Your current subscription plan does not support historical weather data. Please upgrade your account to use this feature."

  Scenario: weather014- Get forecast weather stats with all optional params
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "function_access_restricted"
    And ensure the error response has field "error.code" and has data "105"
    And ensure the error response has field "error.info" and has data "Access Restricted - Your current Subscription Plan does not support this API Function."

  @ignore
  Scenario: weather015- Verify error code 605 Invalid Language
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.type" and has data "invalid_language"
    And ensure the error response has field "error.code" and has data "605"
    And ensure the error response has field "error.info" and has data "An invalid language code was specified."

  Scenario: weather016- Verify Current weather api end point responds to callback function
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather017- Verify historical weather api end point responds to callback function
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather018- Verify historical time series weather api end point responds to callback function
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather019- Verify forecast weather api end point responds to callback function
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather020- Verify error code 606 invalid unit
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    And ensure the error response has field "error.code" and has data "606"
    And ensure the error response has field "error.info" and has data "You have specified an invalid unit. Please try again or refer to our API documentation."

  Scenario: weather021- Verify error code 607 invalid_interval
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather022- Verify error code 608 forecast_days_not_supported_on_plan
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather023- Verify error code 612 invalid_historical_time_frame
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather024- Verify error code 611 invalid_historical_date
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather025- Verify error code 609 invalid_forecast_days
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather026- Verify error code 613 historical_time_frame_too_long
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"

  Scenario: weather027- Verify error code 614 missing_historical_date
    Given the rest url
    When the rest api is hit
    Then ensure the response status code is 200
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
