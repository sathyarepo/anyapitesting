package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class CommonHooks {
	
	public static String testName ="";

	@Before
	public void before(Scenario scenario) {
		testName= scenario.getName().split("-")[0].trim();
	}
}
