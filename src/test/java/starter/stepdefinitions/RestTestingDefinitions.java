package starter.stepdefinitions;

import org.junit.Assert;

import dataproviders.DataProviderClient;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import uilities.RestBuilder;
import net.serenitybdd.rest.SerenityRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestTestingDefinitions {

	private static Logger logger = LoggerFactory.getLogger(RestTestingDefinitions.class);
	private static final String SUCCESS = "success";
	public static Response response;

	@Given("the rest url")
	public void the_rest_url() {
		// Write code here that turns the phrase above into concrete actions
		RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName));
	}

	@When("the rest api is hit")
	public void the_rest_url_is_hit() {
		String apiOperation = "";
		apiOperation = DataProviderClient.exceldataMap.get(CommonHooks.testName).get("operation").toString();
		
		switch (Method.valueOf(apiOperation)) {
		case GET:
			response = SerenityRest.get(RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName))).andReturn();
			break;
		case POST:
			 response = SerenityRest.post(RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName)));
		case PUT:
			response = SerenityRest.put(RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName)));
		case DELETE:
			response = SerenityRest.delete(RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName)));
		case PATCH:
			response = SerenityRest.patch(RestBuilder.getRestURI(DataProviderClient.exceldataMap.get(CommonHooks.testName)));
		default:
			break;
		}
		response.then().assertThat().statusCode(200);
		logger.info("Status Code of the response is: "+response.getStatusCode());
	}

	@Then("ensure the response has field {string} and has data {string}")
	public void ensure_the_field_is(String path, String value) {
		// Write code here that turns the phrase above into concrete actions
		if (response.getBody().jsonPath().get(SUCCESS) != null) {
			logger.info("Value in the field success in error response is: "+response.getBody().jsonPath().get(SUCCESS).toString());
			if (response.getBody().jsonPath().get(SUCCESS).toString().equals("false")) {
				logger.error("Error code is: " + response.getBody().jsonPath().get("error.code").toString());
				Assert.fail("Error: Response didnt matched!! Error code is: " + response.getBody().jsonPath().get("error.code").toString()+" Error info is: "+ response.getBody().jsonPath().get("error.info").toString());
			}
		}
		else if (response.getBody().jsonPath().get(path).toString().equals(value)) {
			logger.info("Validation Success: "+"Value of the field " + path + " : " + response.getBody().jsonPath().get(path).toString());
		} else {
			logger.info("Validation Failure: "+"Value of the field " + path + " is not: " + response.getBody().jsonPath().get(path).toString());
			Assert.fail("Validation Failure: Value of the field " + path + " is not: " + response.getBody().jsonPath().get(path).toString());
		}
	}
	
	@Then("ensure the error response has field {string} and has data {string}")
	public void ensure_the_error_response_field_is(String path, String value) {
		// Write code here that turns the phrase above into concrete actions
		if (response.getBody().jsonPath().get(SUCCESS) != null) {
			logger.info("Value in the field success in error response is: "+response.getBody().jsonPath().get(SUCCESS).toString());
			if (response.getBody().jsonPath().get(SUCCESS).toString().equals("false")) {
				if (response.getBody().jsonPath().get(path).toString().equals(value)) {
					logger.info("Validation Success: Value of the field " + path + " : " + response.getBody().jsonPath().get(path).toString());
				} else {
					logger.error("Validation Failure: Value of the field " + path + " is not: " + response.getBody().jsonPath().get(path).toString());
					Assert.fail("Validation Failure: Value of the field " + path + " is not: " + response.getBody().jsonPath().get(path).toString());
				}
			}
		}else {
			logger.error("Validation Failure: Response is not a error response");
			Assert.fail("Fail!! Response is not a error response");
		}
	}
	
	@Then("ensure the response header has {string} as {string}")
	public void ensure_the_response_header_has_as(String headerName, String headerValue) {
			Assert.assertFalse("Fail!! Response header "+headerName+" value is different. Expected: "+headerValue+" Actual: "+response.getHeader(headerName), !response.getHeader(headerName).toString().equalsIgnoreCase(headerValue));
	}
	
	@And("ensure the response status code is {int}")
	public void ensure_the_response_code(int responseCode) {
		Assert.assertEquals("Fail!! Response status code is not: "+responseCode, response.getStatusCode(), responseCode);
	}
}