package dataproviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import uilities.PropertyReader;

public class DataProviderClient {

	public static FileInputStream fis;
	public static XSSFWorkbook wb;
	public static File excel;
	public static Map<String, Map<String, String>> exceldataMap;
	public static Map<String, String> rowDataMap;
	public static ArrayList<String> mappingMetadata = new ArrayList<String>();
	
	
	
	public static void getDataFromExcel() throws IOException {

		System.out.println("Data Sheet path is: " + System.getProperty("user.dir") + "\\");
		//excel = new File("src/test/resources/datasheet.xlsx");
		excel =  new File(PropertyReader.getInstance().getProperty("data.sheet.path"));
		fis = new FileInputStream(excel);
		wb = new XSSFWorkbook(fis);
		//String sheetName = "Sheet1";
		XSSFSheet sheet = wb.getSheet(PropertyReader.getInstance().getProperty("data.sheet.name"));
		int rowCount = sheet.getPhysicalNumberOfRows();
		int colCount = sheet.getRow(0).getPhysicalNumberOfCells();
		XSSFRow headerRow = sheet.getRow(0);
		exceldataMap = new LinkedHashMap<String, Map<String, String>>();
		for (int i = 1; i < rowCount; i++) {
			rowDataMap = new LinkedHashMap<String, String>();
			for (int j = 0; j < colCount; j++) {
				XSSFCell currentCell = sheet.getRow(i).getCell(j);
				if (currentCell != null) {
					rowDataMap.put(headerRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
				}
			}
			exceldataMap.put(sheet.getRow(i).getCell(0).getStringCellValue(), rowDataMap);
		}
		System.out.println(exceldataMap);
		//System.out.println(RestBuilder.getRestURI(exceldataMap.get("weather001")));


	}

	public static String getDataFromExcelDatamap(String testcaseID, String fieldName) {

		String str = null;
		if (exceldataMap.get(testcaseID).get(fieldName) != null && !Objects.equals(exceldataMap.get(testcaseID).get(fieldName), "")) {
			str = exceldataMap.get(testcaseID).get(fieldName);
		}
		return str;
	}
}
