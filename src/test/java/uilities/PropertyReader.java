package uilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
	private  static PropertyReader propertyReader =  null;
	Properties prop = new Properties();
	private PropertyReader(){
		
	}
	
	public static PropertyReader getInstance() {
		if(propertyReader==null) {
			propertyReader =  new PropertyReader();
			propertyReader.getProperties();
		}
		return propertyReader;
	}
	
	private void getProperties() {
		try(FileInputStream inputStream = new FileInputStream("src/test/resources/config.properties")){
			prop.load(inputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getProperty(String propertyName) {
		return prop.getProperty(propertyName);
		
	}
	
}
