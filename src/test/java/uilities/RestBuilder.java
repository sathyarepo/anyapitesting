package uilities;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RestBuilder {
	
	static String paramPrefix =  PropertyReader.getInstance().getProperty("data.param.prefix");

	public static String getRestURI(Map<String, String> data) {
		System.out.println(data);
		Map<String, String> filteredMap = data.entrySet().stream().filter(x -> x.getKey().contains(paramPrefix))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		return data.get("baseurl") + data.get("endpoint") + "?" + getParams(filteredMap);

	}

	private static String getParams(Map<String, String> data) {

		StringBuilder paramStr = new StringBuilder();
		for (String x : data.keySet()) {
			if (data.get(x) != null && !Objects.equals(data.get(x), "")) {
				if (Objects.equals(paramStr.toString(), "")) {
					paramStr.append(x.replace(paramPrefix, "")).append("=").append(data.get(x));
				} else {
					paramStr.append("&").append(x.replace(paramPrefix, "")).append("=").append(data.get(x));
				}
			}
		}
		return paramStr.toString();

	}

}
