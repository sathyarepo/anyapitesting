**About AnyAPITesting framework**

This AnyAPITesting framework is designed to test the Weather API endpoints but has the capability to test any other APIs as well.

The below service endpoints are tested here:
    
    Current
    
    historical
    
    forecast
    
    autocomplete

The framework is constructed in a such as way that it decouples data from feature files but ensuring that scenario remains logical and readable.

**Technical stack:**

    Serenity v2.4.24

    Cucumber v6.10.2
    
    Java 8 
    
    Rest Assured v4.3.3
    
    Apache Poi v3.17
    
    Maven 

**Project Structure:**
src/test/java

    dataproviders

        DataProviderClient.java
    
    starter
        
        CucumberTestSuite.java
    
    starter.stepdefinitions
    
        CommonHooks.java
        
        RestTestingDefinitions.java
    
    uilities
        
        PropertyReader.java
        
        RestBuilder.java

src/test/resources
    
    features
        
        WeatherAPI
    
            RestTesting.feature
    
    config.properties
    
    datasheet.xlsx
    
    logback-test.xml
    
    serenity.conf

logs
    
    ApiTesting.log


**How to use this framework:**

1. Two main components datasheet.xlsx and Feature files are to be updated by the user.

2. datasheet.xlsx has the all basic information of the apí to be tested and also the parameters that needs to passed in the test. 

3. parameters of the rest api will be there as columns in the datasheet and column header will have the prefix as mentioned in the config.properties 'data.param.prefix'.
    for example: if data.param.prefix = param_ in config.properties file, then parameters column headers will be "param_parameter_name"

4. testcaseid column in the datasheet should match the scenario description prefix in the feature file. 
    for example: testcasedid column has 'test001' , then the corresponding scenario in the feature file will have the description as follows.
    Scenario: test001 - Verify the API with x parameters.

5. Below are the available glues/step definitions built to test the APIs.

| Glue | description |
| ------ | ------ |
| Given the rest url | It builds the rest api URI based on the data in the test data sheet |
| When the rest api is hit | API URL is hit and ensures a response is received |
|Then ensure the response status code is {int}|Validates the response status code id {int}|
|Then ensure the response header has {String} as {String}|Validates the header of the response has the expected value|
|Then ensure the response header has {String} as {String}|Validates the header of the response has the expected value|
|And ensure the response has field {String} and has data {String}|Validates the response field has the expected value|
|And ensure the error response has field {String} and has data {String}|Validates the error response field has the expected value|

**Sample feature file**

Scenario: weather001- Get current weather details

    Given the rest url

    When the rest api is hit
    
    Then ensure the response status code is 200
    
    And ensure the response header has "Content-Type" as "application/json; Charset=UTF-8"
    
    And ensure the response has field "request.type" and has data "City"

**To execute the Test scenarios:**

pre-requisites:

    Ensure config.properties is updated appropriately.

    datasheet.xlsx is updated appropriately. (Especially change the data_access_key data in datasheet with your own access token)
    
    feature file is updated appropriately.

Steps:

    Testrunner 'CucumberTestSuite.java' is available in the 'starter' package.
    
    Run the maven build command 'mvn verify' to trigger the tests.

**Verify Reports:**

    Report will be avaliable here: target/site/serenity/index.html
    
    In GitLabs - Reports can be downloaded as artifact in pipeline tab.

**Log verification:**

    Execution Logs available here: \logs\ApiTesting.log

 

